# -*- coding: utf-8 -*-
"""
Created on Mon Apr  6 18:46:35 2020

@author: 15193
"""
#%% Import libraries
import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
from tqdm import tqdm

#%% Load training data
DATADIR = "C:\\Users\\15193\\OneDrive - The University of Western Ontario\\Didu - Western\\2020-Winter\\Machine Learning Dr.Shami\\Project\\FullyProcessedPlots"
TRAIN_CATEGORIES = ["NonBirdTrain","BirdTrain"]

#Creating training data set
training_data = []

IMG_SIZE = 70
IMG_WIDTH = 95

def create_training_data():
    for train_category in TRAIN_CATEGORIES:  

        path = os.path.join(DATADIR,train_category)  
        class_num = TRAIN_CATEGORIES.index(train_category)  # get the classification  (0 or 1). 0=BirdData 1=NonBirdData

        for img in tqdm(os.listdir(path)):  # iterate over each image
            train_img_array = cv2.imread(os.path.join(path,img) ,cv2.IMREAD_GRAYSCALE)  # convert to array
            train_new_array = cv2.resize(train_img_array, (IMG_WIDTH, IMG_SIZE))  # resize to normalize data size
            training_data.append([train_new_array, class_num])  # add this to our training_data

create_training_data()
print(len(training_data))

#%%Load cross validation data
VAL_CATEGORIES = ["NonBirdCross","BirdCross"]

#Creating validation data set
validation_data = []

def create_validation_data():
    for val_category in VAL_CATEGORIES:  

        path = os.path.join(DATADIR,val_category)  
        class_num = VAL_CATEGORIES.index(val_category)  # get the classification  (0 or 1). 0=BirdData 1=NonBirdData

        for img in tqdm(os.listdir(path)):  # iterate over each image
            val_img_array = cv2.imread(os.path.join(path,img) ,cv2.IMREAD_GRAYSCALE)  # convert to array
            val_new_array = cv2.resize(val_img_array, (IMG_WIDTH, IMG_SIZE))  # resize to normalize data size
            validation_data.append([val_new_array, class_num])  # add this to our validation_data

create_validation_data()
print(len(validation_data))
#%% Load testing data
TEST_CATEGORIES = ["NonBirdTest","BirdTest"]

#Creating testing data set
testing_data = []

def create_testing_data():
    for test_category in TEST_CATEGORIES:  

        path = os.path.join(DATADIR,test_category)  
        class_num = TEST_CATEGORIES.index(test_category)  # get the classification  (0 or 1). 0=BirdData 1=NonBirdData

        for img in tqdm(os.listdir(path)):  # iterate over each image
            test_img_array = cv2.imread(os.path.join(path,img) ,cv2.IMREAD_GRAYSCALE)  # convert to array
            test_new_array = cv2.resize(test_img_array, (IMG_WIDTH, IMG_SIZE))  # resize to normalize data size
            testing_data.append([test_new_array, class_num])  # add this to our testing_data

create_testing_data()
print(len(testing_data))
#%%
#Shuffling training, cross validation and testing data
import random

random.shuffle(training_data)
for sample in training_data[:10]:
    print(sample[1])
    
random.shuffle(validation_data)
for sample in training_data[:10]:
    print(sample[1])
    
random.shuffle(testing_data)
for sample in training_data[:10]:
    print(sample[1])
#%%
#Seperating features and labels of training dataset   
X_train = []
Y_train = []

for features,label in training_data:
    X_train.append(features)
    Y_train.append(label)

#print(X_train[0].reshape(-1, IMG_SIZE, IMG_SIZE, 1))

X_train = np.array(X_train).reshape(-1, IMG_WIDTH, IMG_SIZE, 1)
Y_train = np.array(Y_train)
#%%
#Seperating features and labels of validation dataset   
X_val = []
Y_val = []

for features,label in validation_data:
    X_val.append(features)
    Y_val.append(label)

X_val = np.array(X_val).reshape(-1, IMG_WIDTH, IMG_SIZE, 1)
Y_val = np.array(Y_val)
#%%
#Seperating features and labels of testing dataset   
X_test = []
Y_test = []

for features,label in testing_data:
    X_test.append(features)
    Y_test.append(label)

X_test = np.array(X_test).reshape(-1, IMG_WIDTH, IMG_SIZE, 1)
Y_test = np.array(Y_test)
#%%
#Import packages for NN
import tensorflow as tf
from tensorflow.keras.datasets import cifar10
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten
from tensorflow.keras.layers import Conv2D, MaxPooling2D
#%%
X_train = X_train/255.0
X_test = X_test/255.0
X_val = X_val/255.0

#%%
#Creating the model

model = Sequential()

model.add(Conv2D(32, (3, 3), input_shape=X_train.shape[1:]))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.2))

model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.2))

model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())  

model.add(Dense(256))
model.add(Dropout(0.5))
model.add(Dense(256))
model.add(Dropout(0.5))
model.add(Dense(128))
model.add(Dropout(0.5))
model.add(Dense(128))
model.add(Dropout(0.5))
model.add(Dense(64))
model.add(Dropout(0.5))
model.add(Dense(64))
#model.add(Dropout(0.5))
model.add(Dense(1))
model.add(Activation('sigmoid'))

model.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])
model.summary()
#%%
#Fit the model
#history = model.fit(X_train, Y_train, epochs = 300,
              #verbose=1, validation_data=(X_test, Y_test))
history = model.fit(X_train, Y_train, batch_size=1024, epochs = 500,
              verbose=1, validation_data=(X_val, Y_val))
#%% 
#Evaluate the model
score = model.evaluate(X_test, Y_test, verbose=0)
print('Test score:', score[0])
print('Test accuracy:', score[1])
print(model.predict_classes(X_test[1:10]))
print(Y_test[1:10])

#%% 
#Plot the losses and accuracies
train_loss=history.history['loss']
val_loss=history.history['val_loss']
train_acc=history.history['accuracy']
val_acc=history.history['val_accuracy']
xc=range(500)

plt.figure(1,figsize=(7,5))
plt.plot(xc,train_loss)
plt.plot(xc,val_loss)
plt.xlabel('num of Epochs')
plt.ylabel('loss')
plt.title('train_loss vs val_loss')
plt.grid(True)
plt.legend(['train','val'])
plt.style.use(['classic'])

plt.figure(2,figsize=(7,5))
plt.plot(xc,train_acc)
plt.plot(xc,val_acc)
plt.xlabel('num of Epochs')
plt.ylabel('accuracy')
plt.title('train_acc vs val_acc')
plt.grid(True)
plt.legend(['train','val'],loc=4)
plt.style.use(['classic'])

#%%
Y_pred = model.predict_classes(X_test[:])
print(len(Y_pred))

#%%
#Plot of confusion matrix
from sklearn.metrics import confusion_matrix
import itertools
import matplotlib.pyplot as plt

cm = confusion_matrix(y_true=Y_test, y_pred= Y_pred)

def plot_confusion_matrix(cm, classes,
    normalize=False,
    title='Confusion matrix',
    cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

cm_plot_labels = ['No_Bird_contamination','Bird_contamination']

plot_confusion_matrix(cm=cm, classes=cm_plot_labels, title='Confusion Matrix')

#%%
#Filtered data on CNN
FTEST_CATEGORIES = ["FilterData"]

#Creating training data set
filtered_data = []

IMG_SIZE = 70
IMG_WIDTH = 70

def create_filtered_data():
    for ftest_category in FTEST_CATEGORIES:  

        path = os.path.join(DATADIR,ftest_category)  
        class_num = FTEST_CATEGORIES.index(ftest_category)  # get the classification  (0 or 1). 0=BirdData 1=NonBirdData

        for img in tqdm(os.listdir(path)):  # iterate over each image
            ftest_img_array = cv2.imread(os.path.join(path,img) ,cv2.IMREAD_GRAYSCALE)  # convert to array
            ftest_new_array = cv2.resize(ftest_img_array, (IMG_WIDTH, IMG_SIZE))  # resize to normalize data size
            filtered_data.append([ftest_new_array, class_num])  # add this to our filtered_data

create_filtered_data()
print(len(filtered_data))
#%%
#Seperating features and labels of filtered dataset   
X_ftest = []
Y_ftest = []

for features,label in filtered_data:
    X_ftest.append(features)
    Y_ftest.append(label)

X_ftest = np.array(X_ftest).reshape(-1, IMG_WIDTH, IMG_SIZE, 1)
Y_ftest = np.array(Y_ftest)

X_ftest = X_ftest/255.0

#%% 
#Evaluate the model on filtered data
score = model.evaluate(X_ftest, Y_ftest, verbose=0)
print('Test score:', score[0])
print('Test accuracy:', score[1])
print(model.predict_classes(X_ftest[1:10]))
print(Y_ftest[1:10])

#%%
Y_fpred = model.predict_classes(X_ftest[:])
print(len(Y_fpred))
#%%
#Plot of confusion matrix for filtered data

cm = confusion_matrix(y_true=Y_ftest, y_pred= Y_fpred)

def plot_confusion_matrix(cm, classes,
    normalize=False,
    title='Confusion matrix',
    cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

cm_plot_labels = ['No_Bird_contamination','Bird_contamination']

plot_confusion_matrix(cm=cm, classes=cm_plot_labels, title='Confusion Matrix')
