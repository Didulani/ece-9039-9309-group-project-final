%% Load
clc
clear

birdFiles = dir('BirdData/*.mat');
nBirdFiles = dir('NonBirdData/*.mat');

%% Purge files with rain
for i = length(birdFiles):-1:1
    a = matfile("BirdData/" + birdFiles(i).name, 'Writable', false);
    a1 = a.Header;
    if(a1.PLUIE == 1)
        birdFiles(i,:) = [];
    end
end
for i = length(nBirdFiles):-1:1
    b = matfile("NonBirdData/" + nBirdFiles(i).name, 'Writable', false);
    b1 = b.Header;
    if(b1.PLUIE == 1)
        nBirdFiles(i,:) = [];
    end
end
%% Time Series standard deviations
birdstd = zeros(length(birdFiles),1);
nBirdstd = zeros(length(nBirdFiles),1);
birdSpecstd = zeros(length(birdFiles),1);
nBirdSpecstd = zeros(length(nBirdFiles),1);

for i = 1:length(birdFiles)
    a = matfile("BirdData/" + birdFiles(i).name, 'Writable', false);
    birdstd(i) = std(a.TimeSeries);
    birdSpecstd(i) = std(std(a.Spectrogram));
    
end
for i = 1:length(nBirdFiles)
    b = matfile("NonBirdData/" + nBirdFiles(i).name, 'Writable', false);
    nBirdstd(i) = std(b.TimeSeries);
    nBirdSpecstd(i) = std(std(b.Spectrogram));
end

birdstdMean = mean(birdstd);
nBirdstdMean = mean(nBirdstd);
birdSpecstdMean = mean(birdSpecstd);
nBirdSpecstdMean = mean(nBirdSpecstd);

%%
load('NonBirdData/WP_20190707-1515_beam-5_gate-1644.5m.mat')
pcolor(xr,yr,Spectrogram);shading flat
%plot(Spectra)