clc;
clear all;
files = dir('*.mat');
% Loop through each file 
for id = 1:length(files)
    [~, f,ext] = fileparts(files(id).name)
    load([f '.mat']);
    figure();pcolor(xr,yr,Spectrogram);shading flat;
    
    filename = [f '.png'];
    saveas(gcf,filename);
    close all
end

