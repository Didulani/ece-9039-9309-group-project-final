birdFiles = dir('BirdData/*.mat');

for n = 1:length(birdFiles)
    % Load image
    sample = matfile("BirdData/" + birdFiles(n).name, 'Writable', false);
    spectrogram = sample.Spectrogram;
    xr = sample.xr;
    yr = sample.yr;
    
    % Flatten image
    flatSpec = spectrogram;
    flatSpec(flatSpec<10)=0; % Anything below a power of 10 is automatically noise
    activation = 0.60*(sum(flatSpec, 'all')/nnz(flatSpec)); % Our activation threshold
    flatSpec(flatSpec<activation)=0; % Anything below 60% of the average remaining data points is also noise
    flatSpec(flatSpec>activation)=1;
    
    % Separate signal and noise
    signal = spectrogram.*flatSpec; % Extract 'Signal' graph
    noise = spectrogram - signal; % Extract 'Noise' graph
    
    % Determine how much of each row is signal
    PCA = zeros(1,600);
    for i = 1:600
        PCA(i) = mean(flatSpec(i,:)); % The mean shows activation percentage
    end
    
    % Cull signals beneath threshold
    cull = spectrogram - noise; % Noiseless graph of only the signal values
    thresh = 0.75;
    for i = 1:600
        if(PCA(i) < thresh)
            cull(i,:) = zeros(1,544); % If less than 85% of the row is active, erase signals on that row
        end
    end
    
    % Add noise back in
    Spectrogram = cull + noise;
    
    % Save to file
    % "FilterData" folder must exist!
    save("FilterData/"+birdFiles(n).name(1:end-7)+"_Filtered","Spectrogram","xr","yr")
end