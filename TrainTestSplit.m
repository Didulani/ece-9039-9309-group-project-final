%% Load Data
birdFiles = dir('BirdData/*.mat');
nBirdFiles = dir('NonBirdData/*.mat');

%% Rectify class imbalance
n1 = length(birdFiles);
n2 = length(nBirdFiles);
classSample = randsample(n2, n1);
nBirdFiles = nBirdFiles(classSample);

%% Split Bird Files
n = length(birdFiles);
birdTrainSample = randsample(n, 0.6*n); % 60 percent of data
birdTrainFiles = birdFiles(birdTrainSample);
birdFiles(birdTrainSample) = [];
n = length(birdFiles);
birdTestSample = randsample(n, 0.5*n); % 50 percent of 40 percent = 20 percent of data
birdTestFiles = birdFiles(birdTestSample);
birdFiles(birdTestSample) = [];
birdCrossFiles = birdFiles;

%% Split Non-Bird Files
n = length(nBirdFiles);
nBirdTrainSample = randsample(n, 0.6*n); % 60 percent of data
nBirdTrainFiles = nBirdFiles(nBirdTrainSample);
nBirdFiles(nBirdTrainSample) = [];
n = length(nBirdFiles);
nBirdTestSample = randsample(n, 0.5*n); % 50 percent of 40 percent = 20 percent of data
nBirdTestFiles = nBirdFiles(nBirdTestSample);
nBirdFiles(nBirdTestSample) = [];
nBirdCrossFiles = nBirdFiles;

%% Copy files into destination.
for i = 1:length(birdTrainFiles)
    copyfile("BirdData/"+birdTrainFiles(i).name, 'BirdTrain');
end
for i = 1:length(birdCrossFiles)
    copyfile("BirdData/"+birdCrossFiles(i).name, 'BirdCross');
end
for i = 1:length(birdTestFiles)
    copyfile("BirdData/"+birdTestFiles(i).name, 'BirdTest');
end
for i = 1:length(nBirdTrainFiles)
    copyfile("NonBirdData/"+nBirdTrainFiles(i).name, 'NonBirdTrain');
end
for i = 1:length(nBirdCrossFiles)
    copyfile("NonBirdData/"+nBirdCrossFiles(i).name, 'NonBirdCross');
end
for i = 1:length(nBirdTestFiles)
    copyfile("NonBirdData/"+nBirdTestFiles(i).name, 'NonBirdTest');
end